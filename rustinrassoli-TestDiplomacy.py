#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve
        
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\nB Austin Move Madrid\n"
        army, city = diplomacy_read(s)
        self.assertEqual(army['A']['home_city'], "Madrid")
        self.assertEqual(army['B']['home_city'], "Austin")
        self.assertEqual(city['Madrid'], 'A')
        self.assertEqual(city['Austin'], 'B')
        pass


    # ----
    # eval
    # ----

    def test_eval_1(self):
        armies = {
            "A": {"home_city": "Madrid", "action": "Hold", "support": 0, "alive": True},
            "B": {"home_city": "Austin", "action": "Move", "city": "Madrid", "support": 0, "alive": True}  
        }
        cities = {
            "Madrid": "A",
            "Austin": "B"
        }
        states = diplomacy_eval(armies, cities)
        self.assertEqual(states[0], "A [dead]")
        self.assertEqual(states[1], "B [dead]")

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Austin Support C\nC London Move Madrid\nD NewYork Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Madrid\nD London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Austin Support A\nC London Move Madrid\nD NewYork Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]\nD London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC Austin Support B\nD London Move Barcelona\nE Dallas Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC Austin\nD [dead]\nE [dead]\n")
        

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
